package Coplind;

import java.util.*;

/**
 * система голосования по 3 типам Кондорсе
 */
public class Coplind {

    private static List<Candidat> candidats = new LinkedList<Candidat>(){{
       add(new Candidat("Никита", 0));
       add(new Candidat("Ирина", 0));
       add(new Candidat("Саша", 0));
    }};

    public static void main(String[] args) throws Exception {
        System.out.println("Выбор старосты группы. метод Копленда");
        System.out.println("Введите количество голосующих людей");


        Scanner scanner = new Scanner(System.in);
        Integer electorCount = scanner.nextInt();

        System.out.println("Голосуйте " + electorCount + " раз(а)");

        //вывод на просмотр
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(i + " - " + candidats.get(i).getName());
        }

        //голосование n раз
        for (int i = 0; i < electorCount; i++){

            System.out.println("===============================" );
            System.out.println("голосует " + i + "-й избератель" );

            HashMap<Integer, Set<Integer>> electorVote = new HashMap<>();//набор изберателя по всем кандидатам

            for (int j = 0; j < candidats.size(); j++) {
                Candidat candidatA = candidats.get(j);
                Candidat candidatB = new Candidat();

                for (int k = j + 1; k < candidats.size(); k++) { //вывод пар кандидатов
                    candidatB = candidats.get(k);


                    System.out.println("Из пары кандидатов выбирете лучшего (1 или 2). введите 0 если они равны" );
                    System.out.println("Кандидат 1: " + candidatA.getName());
                    System.out.println("Кандидат 2: " + candidatB.getName());

                    Integer vote = scanner.nextInt();

                    if (vote == 1){

                        //первый лучше второго
                        candidatA.setRating(candidatA.getRating() + 1);
                        candidatB.setRating(candidatB.getRating() - 1);

                    } else if (vote == 2){
                        // второй лучше первого
                        candidatB.setRating(candidatB.getRating() + 1);
                        candidatA.setRating(candidatA.getRating() - 1);
                    } else if (vote == 0){
                        // равны
                    }

                }



            }

        }



        //вывод на просмотр с их рейтингом
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(
                    i
                    + " - "
                    + candidats.get(i).getName()
                    + ", рейтинг: "
                    + candidats.get(i).getRating()
            );
        }

        //поиск кандидата, который никому не уступал в сравнении (абсолютного победителя)
        Integer maxRating = candidats.stream().mapToInt(Candidat::getRating).max().orElseThrow(() -> new Exception("рейтинг не найден"));

        Candidat winner = candidats.stream().filter(candidat -> candidat.getRating().equals(maxRating)).findFirst().orElseThrow(() -> new Exception("Нет победителя"));
        System.out.println("Победил кандидат: " + winner.getName() + ", так как его рейтинг по правилу Копленда больше остальных.");

    }
}
