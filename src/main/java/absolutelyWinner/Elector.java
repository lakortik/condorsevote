package absolutelyWinner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Elector {

    private HashMap<Integer, Set<Integer>> candidateIdAndBetterSet;//
}
