package absolutelyWinner;

import java.util.*;

/**
 * система голосования по 3 типам Кондорсе
 */
public class AbsolutelyWinner {

    private static List<Candidat> candidats = new LinkedList<Candidat>(){{
       add(new Candidat("Никита", 0));
       add(new Candidat("Ирина", 0));
       add(new Candidat("Саша", 0));
    }};

    private static List<Elector> electors = new LinkedList<Elector>();

    public static void main(String[] args) throws Exception {
        System.out.println("Выбор старосты группы. метод относительного большинства");
        System.out.println("Введите количество голосующих людей");


        Scanner scanner = new Scanner(System.in);
        Integer electorCount = scanner.nextInt();

        System.out.println("Голосуйте " + electorCount + " раз(а)");

        //вывод на просмотр
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(i + " - " + candidats.get(i).getName());
        }

        //голосование n раз
        for (int i = 0; i < electorCount; i++){

            System.out.println("===============================" );
            System.out.println("голосует " + i + "-й избератель" );

            HashMap<Integer, Set<Integer>> electorVote = new HashMap<>();//набор изберателя по всем кандидатам

            for (int j = 0; j < candidats.size(); j++) {
                Candidat candidatA = candidats.get(j);
                Candidat candidatB = new Candidat();

                for (int k = j + 1; k < candidats.size(); k++) { //вывод пар кандидатов
                    candidatB = candidats.get(k);


                    System.out.println("Из пары кандидатов выбирете лучшего (1 или 2)" );
                    System.out.println("Кандидат 1: " + candidatA.getName());
                    System.out.println("Кандидат 2: " + candidatB.getName());

                    Integer vote = scanner.nextInt();

                    if (vote == 1){

                        Set<Integer> newSet = new HashSet<>();
                        if (electorVote.get(j) != null){
                            newSet = electorVote.get(j);
                        }
                        newSet.add(k);

                        electorVote.put(j, newSet);//первый лучше второго

                    } else if (vote == 2){
                        Set<Integer> newSet = new HashSet<>();
                        if (electorVote.get(k) != null){
                            newSet = electorVote.get(k);
                        }
                        newSet.add(j);

                        electorVote.put(k, newSet);// второй лучше первого
                    }

                }



            }
            electors.add(new Elector(electorVote));

        }

        for (Elector elector : electors) {//каждый избератель
            for (int i = 0; i < candidats.size(); i++) {//каждый кандидат
                boolean presentInBetterSet = false;
                for (int j = 0; j < candidats.size(); j++) {//каждый кандидат сверяется в изберателе
                    if (elector.getCandidateIdAndBetterSet().get(j) != null
                            && elector.getCandidateIdAndBetterSet().get(j).contains(i)
                    ){
                        //кандидат найден как хуже кого-то
                        candidats.get(i).setThisCandidatLoseCountByElectorVote(candidats.get(i).getThisCandidatLoseCountByElectorVote() + 1);
                    }
                }
//                if (presentInBetterSet) {
//                    увеличили счетчик побед над другими у кандидата в итерации
//                }
            }
        };

        //вывод на просмотр с количетвом кандидатов, которым текущий проиграл в сравнении
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(
                    i
                    + " - "
                    + candidats.get(i).getName()
                    + ", количество превзошедших в попарном сравнении: "
                    + candidats.get(i).getThisCandidatLoseCountByElectorVote()
            );
        }

        //поиск кандидата, который никому не уступал в сравнении (абсолютного победителя)
        Candidat winner = candidats.stream().filter(candidat -> candidat.getThisCandidatLoseCountByElectorVote().equals(0)).findFirst().orElseThrow(() -> new Exception("Нет абсолютного победителя"));
        System.out.println("Победил кандидат: " + winner.getName() + ", так как в любом сравнении оказывался победителем.");

    }
}
