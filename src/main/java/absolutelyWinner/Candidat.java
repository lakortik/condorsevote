package absolutelyWinner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Candidat {

    private String name;
    private Integer thisCandidatLoseCountByElectorVote;//скольки изберателей считают этого хуже кого-то
}
