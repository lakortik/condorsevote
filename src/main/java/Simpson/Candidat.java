package Simpson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class Candidat {

    public static Integer idSeq = 0;
    private Integer id;
    private String name;
    private Integer rating;

    public Candidat(String name, Integer rating){
        this.id = idSeq;
        idSeq++;

        this.name = name;
        this.rating = rating;
    };

    public Candidat(){
        this.id = idSeq;
        idSeq++;

    };
}

