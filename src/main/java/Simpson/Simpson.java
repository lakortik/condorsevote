package Simpson;

import java.util.*;

/**
 * система голосования по 3 типам Кондорсе
 */
public class Simpson {

    private static List<Candidat> candidats = new LinkedList<Candidat>(){{
       add(new Candidat("Никита", 0));
       add(new Candidat("Ирина", 0));
       add(new Candidat("Саша", 0));
    }};

    public static void main(String[] args) throws Exception {
        int[][] candidatsVotes = new int[candidats.size()][candidats.size()];//матрица голосования

        //заполнение матрицы
        for (int[] candidatVotes : candidatsVotes) {
            Arrays.fill(candidatVotes, 0);
        }



        
        
        System.out.println("Выбор старосты группы. метод Симпсона (минимаксный критерий)");
        System.out.println("Введите количество голосующих людей");


        Scanner scanner = new Scanner(System.in);
        Integer electorCount = scanner.nextInt();

        System.out.println("Голосуйте " + electorCount + " раз(а)");

        //вывод на просмотр
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(i + " - " + candidats.get(i).getName());
        }


        //голосование n раз
        for (int i = 0; i < electorCount; i++){

            System.out.println("===============================" );
            System.out.println("голосует " + i + "-й избератель" );

            HashMap<Integer, Set<Integer>> electorVote = new HashMap<>();//набор изберателя по всем кандидатам

            for (int j = 0; j < candidats.size(); j++) {
                Candidat candidatA = candidats.get(j);
                Candidat candidatB = new Candidat();

                for (int k = j + 1; k < candidats.size(); k++) { //вывод пар кандидатов
                    candidatB = candidats.get(k);


                    System.out.println("Из пары кандидатов выбирете лучшего (1 или 2). введите 0 если они равны" );
                    System.out.println("Кандидат 1: " + candidatA.getName());
                    System.out.println("Кандидат 2: " + candidatB.getName());

                    Integer vote = scanner.nextInt();

                    if (vote == 1){

                        //первый лучше второго
                        candidatsVotes[j][k]++;

                    } else if (vote == 2){
                        // второй лучше первого
                        candidatsVotes[k][j]++;

                    }

                }



            }

        }

        //вывод матрицы
        for (int[] candidatVotes : candidatsVotes) {
            for (int candidatVote : candidatVotes) {
                System.out.print(candidatVote + " ");
            }
            System.out.println();
//            System.out.println(i);

        }

        for (int i = 0; i < candidatsVotes.length; i++) {
            int[] candidatVotes = candidatsVotes[i];

            int minInIteration = candidatsVotes[i][i == 0 ? 1 : 0];//инициализируем переменную из мартицы. исключая главную диагоняль
            for (int i1 = 0; i1 < candidatsVotes[i].length; i1++) {
                if (i == i1){continue;}
                minInIteration = Math.min(candidatsVotes[i][i1], minInIteration);
            }
            candidats.get(i).setRating(minInIteration);
        }




        //вывод на просмотр с их рейтингом
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(
                    i
                    + " - "
                    + candidats.get(i).getName()
                    + ", рейтинг: "
                    + candidats.get(i).getRating()
            );
        }

        //поиск кандидата, который никому не уступал в сравнении (абсолютного победителя)
        Integer maxRating = candidats.stream().mapToInt(Candidat::getRating).max().orElseThrow(() -> new Exception("рейтинг не найден"));

        if (candidats.stream().filter(candidat -> candidat.getRating().equals(maxRating)).count() > 1){
            System.out.println("Нет одназначного победителя. несколько кандидатов набрали рейтинг равный " + maxRating);
        } else {
            Candidat winner = candidats.stream().filter(candidat -> candidat.getRating().equals(maxRating)).findFirst().orElseThrow(() -> new Exception("Нет победителя"));
            System.out.println("Победил кандидат: " + winner.getName() + ", так как его рейтинг по правилу Симпсона больше остальных.");
        }
    }
}
